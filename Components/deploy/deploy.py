from sklearn.external import joblib

class order_classifier(object):

    def __init__(self):
        self.model = joblib.load('Orderextension_classifier.sav')
        self.class_names = ["Will be extended", "Not extended"];
        
    def predict(self, X,features_names):
        return self.model.predict_proba(X)