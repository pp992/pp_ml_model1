import pandas as pd
import numpy as np
from sklearn.ensemble import RandomForestClassifier
from sklearn.pipeline import Pipeline
from sklearn.externals import joblib

def main():
	clf=RandomForestClassifier()
	p=Pipeline([('clf',clf)])
	print('Training..')
	p.fit(x,y)
	print('Model trained')
	
	filename_p='Orderextension_classifier.sav'
	print('saving model in %s' % filename_p)
	joblib.dump(p, filename_p)
	print('Model saved!')
	
if __name__ == "__main__":
	print('Loading data')
	X='enter bucket url here'
	y='enter bucket url here'
	print('Dataset loaded')
	main()