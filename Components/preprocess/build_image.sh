#!/bin/bash -e

image_name=gcr.io/google-containers/python:3.5.1-alpine
image_tag=latest

full_image_name=${image_name}:${image_tag}
base_image_tag=3.5.1-alpine

cd "$(dirname "$0")" 

docker build --build-arg BASE_IMAGE_TAG=${base_image_tag} -t "${full_image_name}" .
docker push "$full_image_name"

docker inspect --format="{{index .RepoDigests 0}}" "${IMAGE_NAME}"
