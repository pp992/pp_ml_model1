import pandas as pd
import numpy as np
df=pd.read_csv('https://drive.google.com/file/d/1cZuMk0E6QQLC0pdcOAiRy7lOsA1TEury/view?usp=sharing')

def preprocess(df):
    df['ShortName']=df['ShortName'].astype('str')
    df['ShortName']=df['ShortName'].astype('str')+',0'
    s=df['ShortName'].str.split(',',expand=True)
    df=df.drop(['ShortName'],axis=1)
    df=pd.concat([s,df],axis=1)
    df['created_time']=df['Created_OrderPeriods'].str[11:13]+df['Created_OrderPeriods'].str[14:16]
    df['starttime_time']=df['StartTime'].str[11:13]+df['StartTime'].str[14:16]
    df['endtime_time']=df['EndTime'].str[11:13]+df['EndTime'].str[14:16]
    df['created_month']=df['Created_OrderPeriods'].str[5:7]
    df['starttime_month']=df['StartTime'].str[5:7]
    df['endtime_month']=df['EndTime'].str[5:7]
    df['created_day']=df['Created_OrderPeriods'].str[8:10]
    df['starttime_day']=df['StartTime'].str[8:10]
    df['endtime_day']=df['EndTime'].str[8:10]
    df['created_day']=df['created_day'].astype("float")
    df['starttime_day']=df['starttime_day'].astype("float")
    df['endtime_day']=df['endtime_day'].astype("float")
    df['endtime_time']=df['endtime_time'].astype("float")
    df['starttime_time']=df['starttime_time'].astype("float")
    df['created_time']=df['created_time'].astype("float")
    df['created_month']=df['created_month'].astype("float")
    df['starttime_month']=df['starttime_month'].astype("float")
    df['endtime_month']=df['endtime_month'].astype("float")
    df['Created_OrderPeriods']=pd.to_datetime(df['Created_OrderPeriods'])
    df['StartTime']=pd.to_datetime(df['StartTime'])
    df['EndTime']=pd.to_datetime(df['EndTime'])
    df['Created_DOW']=df['Created_OrderPeriods'].dt.dayofweek
    df['StartTime_DOW']=df['StartTime'].dt.dayofweek
    df['EndTime_DOW']=df['EndTime'].dt.dayofweek
    df['Created_Week']=df['Created_OrderPeriods'].dt.week
    df['StartTime_Week']=df['StartTime'].dt.week
    df['EndTime_Week']=df['EndTime'].dt.week
    df[["EndTime_DOW", "StartTime_DOW", "Created_DOW","EndTime_Week", "StartTime_Week", "Created_Week"]] = df[["EndTime_DOW", "StartTime_DOW", "Created_DOW","EndTime_Week", "StartTime_Week", "Created_Week"]].astype("float")
    df=df.rename(columns={0:"One",1:"Two",2:"Three",3:"Four",4:"Five",5:"Six",6:"Seven",7:"Eight"})
    def two():
        if 'Two' in df.columns:
            pass
        else:
            df['Two']=0
    def three():
        if 'Three' in df.columns:
            pass
        else:
            df['Three']=0
    def four():
        if 'Four' in df.columns:
            pass
        else:
            df['Four']=0
    def five():
        if 'Five' in df.columns:
            pass
        else:
            df['Five']=0
    def six():
        if 'Six' in df.columns:
            pass
        else:
            df['Six']=0
    def seven():
        if 'Seven' in df.columns:
            pass
        else:
            df['Seven']=0
    def eight():
        if 'Eight' in df.columns:
            pass
        else:
            df['Eight']=0
    df[["One","Two","Three","Four","Five","Six","Seven","Eight"]]=df[["One","Two","Three","Four","Five","Six","Seven","Eight"]].astype('float')
    df=df[['School', 'OrderCategory', 'OrderSource', 'ID_Order', 'One','Two','Three','Four','Five','Six','Seven','Eight', 'created_time', 'starttime_time', 'endtime_time', 'created_month', 'starttime_month', 'endtime_month', 'created_day', 'starttime_day', 'endtime_day', 'Created_DOW', 'StartTime_DOW', 'EndTime_DOW', 'Created_Week', 'StartTime_Week', 'EndTime_Week']]
    return df